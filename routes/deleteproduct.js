var express = require('express');
var passport = require('passport');
var Account = require('../models/account');
var Product = require('../models/product');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res) {
	Account.find({ username: req.user.username }, function(err, account) {
		if (err) throw err;
		
		Product.find({}, function(err, products) {
			if (err) throw err;

			res.render('deleteproduct', { layout: 'layoutadmin', user : account[0], mang : products });
		});
	});
});

router.post('/', function(req, res) {
	Account.find({ username: req.user.username }, function(err, account) {
		if (err) throw err;

		Product.findOneAndRemove({ name: req.body.name }, function(err) {
			if (err) throw err;
			
			Product.find({}, function(err, products) {
				if (err) throw err;

				res.render('deleteproduct', { layout: 'layoutadmin', user : account[0], mang : products, text: 'Xóa sản phẩm thành công' });
			});

		});

	});
});
module.exports = router;
