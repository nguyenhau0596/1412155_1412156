var express = require('express');
var passport = require('passport');
var Account = require('../models/account');
var Product = require('../models/product');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res) {

	Product.find({ label: 'HTC' }, function(err, product) {
		if (err) throw err;

		res.render('products_list', { layout: 'layout', user : req.user, sp : product });
	});
});

module.exports = router;

	