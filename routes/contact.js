var express = require('express');
var passport = require('passport');
var Account = require('../models/account');
var Product = require('../models/product');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res) {
	res.render('contact', { layout: 'layout', user : req.user });
});

module.exports = router;
