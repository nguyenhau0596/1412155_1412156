var express = require('express');
var passport = require('passport');
var Account = require('../models/account');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res) {
	Account.find({ username: req.user.username }, function(err, account) {
		if (err) throw err;
		
		Account.find({}, function(err, accounts) {
			if (err) throw err;

			res.render('updateaccount', { layout: 'layoutadmin', user : account[0], mang : accounts });
		});
	});
});

router.post('/', function(req, res) {
	Account.find({ username: req.user.username }, function(err, account) {
		if (err) throw err;

		Account.find({ username: req.body.username }, function(err, account1) {
			if (err) throw err;

			res.render('profileuser2', { layout: 'layoutadmin', user : account[0], us : account1[0] });
		});

	});
});
module.exports = router;
