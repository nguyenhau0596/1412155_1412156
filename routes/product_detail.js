var express = require('express');
var passport = require('passport');
var Product = require('../models/product');
var Account = require('../models/account');
var Comment = require('../models/comment');
var router = express.Router();

router.get('/', function(req, res, next) {
	
	var i = req.query.id;
	
	Product.find({ name: i }, function(err, product) {
	  if (err) throw err;
	  Comment.find({ Product: i }, function(err, cmts) {
		if (err) throw err;
	  Product.find({ label : product[0].label }, function(err, relatedsp) {
		if (err) throw err;
			res.render('product_detail', { layout: 'layout', user : req.user, sp : product[0], related : relatedsp , cmt : cmts, idtemp : i});
	}).limit(4);
	});
});
});
												
module.exports = router;
