var express = require('express');
var passport = require('passport');
var Account = require('../models/account');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res) {
	Account.find({ username: req.user.username }, function(err, account) {
	  if (err) throw err;

	  res.render('adminregister', { layout: 'layoutadmin', user : account[0] });
	});
});

router.post('/', function(req, res) {
    Account.register(new Account({ Firstname : req.body.Firstname, Lastname : req.body.Lastname, username : req.body.username, Phone : req.body.Phone, Gender : req.body.radio, Image : req.body.img, Type : req.body.Type}), req.body.password, function(err, account) {
        if (err) {
            Account.find({ username: req.user.username }, function(err, account) {
			  if (err) throw err;

			  res.render('adminregister', { layout: 'layoutadmin', user : account[0], text: 'Đăng ký tài khoản thất bại' });
			});
        }

		Account.find({ username: req.user.username }, function(err, account) {
		  if (err) throw err;

		  res.render('adminregister', { layout: 'layoutadmin', user : account[0], text: 'Đăng ký tài khoản thành công' });
		});
    });
});
module.exports = router;
