var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Product = new Schema({
    name: {
		type: String,
		unique: true
	},
	label: String,
	price: String,
	config: String,
	img: String,
});

module.exports = mongoose.model('Product', Product);