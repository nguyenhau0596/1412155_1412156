var express = require('express');
var passport = require('passport');
var Account = require('../models/account');
var Product = require('../models/product');
var router = express.Router();

/* GET home page. */
router.post('/', function(req, res) {
	var t = req.body.keyword;
	console.log(t);
	var h = req.body.label_selected; 
	console.log(h);
	console.log(req.body.checksearch);
	if(req.body.checksearch =="on") 
	{
		Product.find({ name: {$regex : ".*"+t+".*"}, label : h }, function(err, product) {
			if (err) throw err;
			res.render('search', { layout: 'layout', user : req.user, sp : product });
		});
	}
	else
	{
		Product.find({ name: {$regex : ".*"+t+".*"}}, function(err, product) {
			if (err) throw err;
			res.render('search', { layout: 'layout', user : req.user, sp : product });
		});
	}
});

router.get('/', function(req, res) {
	res.render('search', { layout: 'layout', user : req.user });	
});

module.exports = router;

	