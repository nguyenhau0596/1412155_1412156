var express = require('express');
var passport = require('passport');
var Account = require('../models/account');
var Product = require('../models/product');
var Cart = require('../models/cart');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res) {
	var ten;
	if (req.user) ten = req.user.Firstname;
	else ten ="kk";
	console.log(ten);
	
	Cart.find({ Username: ten }, function(err, sp) {
		if (err) throw err;		
		res.render('cart', { layout: 'layout', user : req.user, spcart : sp });
	});
});

module.exports = router;

	