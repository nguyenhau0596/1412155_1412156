var express = require('express');
var passport = require('passport');
var Account = require('../models/account');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res) {
	Account.find({ username: req.user.username }, function(err, account) {
	  if (err) throw err;

	  res.render('profile', { layout: 'layoutadmin', user : account[0] });
	});
  
});

router.post('/', function(req, res) {
	Account.findOneAndUpdate({ username: req.user.username }, { Firstname : req.body.Firstname, Lastname : req.body.Lastname, Phone : req.body.Phone, Gender: req.body.Gender, Image : req.body.img }, function(err, account) {
		if (err) throw err;

		Account.find({ username: req.user.username }, function(err, account1) {
			if (err) throw err;

			res.render('profile', { layout: 'layoutadmin', user : account1[0] });
		});
	});
});
module.exports = router;
