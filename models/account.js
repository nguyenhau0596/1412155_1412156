var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var passportLocalMongoose = require('passport-local-mongoose');

var Account = new Schema({
    Firstname: String,
	Lastname: String,
	username: String,
	Phone: String,
	Gender: String,
	Image: String,
	Type: Boolean,
    password: String
});

Account.plugin(passportLocalMongoose);

module.exports = mongoose.model('Account', Account);