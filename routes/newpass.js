var express = require('express');
var passport = require('passport');
var Account = require('../models/account');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res) {
  res.render('newpass', { });
});

router.post('/', function(req, res) {
    Account.register(new Account({ Firstname : req.body.Firstname, Lastname : req.body.Lastname, username : req.body.username, Phone : req.body.Phone, Gender : req.body.radio, Image : req.body.img, Type : true}), req.body.password, function(err, account) {
        if (err) {
            return res.render('register', { account : account });
        }

        res.redirect('/login');
    });
});
module.exports = router;
