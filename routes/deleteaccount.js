var express = require('express');
var passport = require('passport');
var Account = require('../models/account');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res) {
	Account.find({ username: req.user.username }, function(err, account) {
		if (err) throw err;
		
		Account.find({}, function(err, accounts) {
			if (err) throw err;

			res.render('deleteaccount', { layout: 'layoutadmin', user : account[0], mang : accounts });
		});
	});
});

router.post('/', function(req, res) {
	Account.find({ username: req.user.username }, function(err, account) {
		if (err) throw err;

		if (req.user.username === req.body.username)
		{
			Account.find({}, function(err, accounts) {
				if (err) throw err;

				res.render('deleteaccount', { layout: 'layoutadmin', user : account[0], mang : accounts, text: 'Tài khoản đang sử dụng, xóa thất bại' });
			});
		}
		
		else
		{
			Account.findOneAndRemove({ username: req.body.username }, function(err) {
				if (err) throw err;
				
				Account.find({}, function(err, accounts) {
					if (err) throw err;

					res.render('deleteaccount', { layout: 'layoutadmin', user : account[0], mang : accounts, text: 'Xóa tài khoản người dùng thành công' });
				});

			});
		}

	});
});
module.exports = router;
