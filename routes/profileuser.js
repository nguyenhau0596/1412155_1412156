var express = require('express');
var passport = require('passport');
var Account = require('../models/account');
var Product = require('../models/product');
var router = express.Router();

router.get('/', function(req, res) {
	Account.find({ username: req.user.username }, function(err, account) {
	  if (err) throw err;

	  res.render('profileuser', { layout: 'layout', us : account[0] });
	});
  
});

router.post('/', function(req, res) {
	Account.findOneAndUpdate({ username: req.user.username }, { Firstname : req.body.Firstname, Lastname : req.body.Lastname, Phone : req.body.Phone, Gender: req.body.Gender, Image : req.body.img }, function(err, account) {
		if (err) throw err;

		Account.find({ username: req.user.username }, function(err, account1) {
			if (err) throw err;

			res.render('profileuser', { layout: 'layout', us : account1[0], text: 'Cập nhật thông tin thành công' });
		});
	});
});

module.exports = router;
