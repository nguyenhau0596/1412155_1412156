var express = require('express');
var passport = require('passport');
var Account = require('../models/account');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res) {
    res.render('login', { user : req.user });
});

router.post('/', passport.authenticate('local', {failureRedirect: '/login'}), (req, res) =>
												{
													/*Nếu true thì customer*/
													if(req.user.Type === true)
													{
														res.redirect('/homepage');
													}
													/*Nếu false thì admin*/
													if(req.user.Type === false)
													{
														res.redirect('/homepageadmin');
													}
												});

												
module.exports = router;
