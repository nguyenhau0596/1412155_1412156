var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var mongoose = require('mongoose');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;

var index = require('./routes/index');
var users = require('./routes/users');
var login = require('./routes/login');
var logout = require('./routes/logout');
var register = require('./routes/register');
var admin = require('./routes/admin');
var profile = require('./routes/profile');
var addproduct = require('./routes/addproduct');
var updateproduct = require('./routes/updateproduct');
var updateproduct2 = require('./routes/updateproduct2');
var deleteproduct = require('./routes/deleteproduct');
var adminregister = require('./routes/adminregister');
var updateaccount = require('./routes/updateaccount');
var profileuser = require('./routes/profileuser');
var deleteaccount = require('./routes/deleteaccount');
var samsung = require('./routes/samsung');
var apple = require('./routes/apple');
var sony = require('./routes/sony');
var htc = require('./routes/htc');
var oppo = require('./routes/oppo');
var lg = require('./routes/lg');
var asus = require('./routes/asus');
var lenovo = require('./routes/lenovo');
var search = require('./routes/search');
var product_detail = require('./routes/product_detail');
var contact = require('./routes/contact');
var addcomment = require('./routes/addcomment');
var cart = require('./routes/cart');
var addcart = require('./routes/addcart');
var profileuser2 = require('./routes/profileuser2');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(require('express-session')({
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: false
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', index);
app.use('/homepage', index);
app.use('/users', users);
app.use('/login', login);
app.use('/logout', logout);
app.use('/register', register);
app.use('/homepageadmin', admin);
app.use('/profile', profile);
app.use('/addproduct', addproduct);
app.use('/update', updateproduct);
app.use('/updatedetail', updateproduct2);
app.use('/delete', deleteproduct);
app.use('/adminregister', adminregister);
app.use('/updateaccount', updateaccount);
app.use('/profileuser', profileuser);
app.use('/deleteaccount', deleteaccount);
app.use('/samsung', samsung);
app.use('/apple', apple);
app.use('/sony', sony);
app.use('/htc', htc);
app.use('/oppo', oppo);
app.use('/lg', lg);
app.use('/asus', asus);
app.use('/lenovo', lenovo);
app.use('/search', search);
app.use('/product_detail', product_detail);
app.use('/contact', contact);
app.use('/addcomment', addcomment);
app.use('/cart', cart);
app.use('/addcart', addcart);
app.use('/profileuser2', profileuser2);

// passport config Customer
var Account = require('./models/account');
passport.use(new LocalStrategy(Account.authenticate()));
passport.serializeUser(Account.serializeUser());
passport.deserializeUser(Account.deserializeUser());

// mongoose
mongoose.connect('mongodb://localhost/passport_local_mongoose_express4');

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
