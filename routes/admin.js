var express = require('express');
var passport = require('passport');
var Account = require('../models/account');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res) {
	if (req.isAuthenticated()){
		res.render('admin', {layout: 'layoutadmin', user: req.user});
	} else {
		res.send('ban chua dang nhap');
	} 
});
												
module.exports = router;
