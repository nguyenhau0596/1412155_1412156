var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var passportLocalMongoose = require('passport-local-mongoose');

var Account_Admin = new Schema({
    Firstname: String,
	Lastname: String,
	username: String,
	Phone: String,
	Gender: String,
    password: String
});

Account_Admin.plugin(passportLocalMongoose);

module.exports = mongoose.model('Account_Admin', Account_Admin);