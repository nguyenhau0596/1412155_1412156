var express = require('express');
var passport = require('passport');
var Account = require('../models/account');
var Product = require('../models/product');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res) {
	Account.find({ username: req.user.username }, function(err, account) {
	  if (err) throw err;

	  res.render('addproduct', { layout: 'layoutadmin', user : account[0] });
	});
});

router.post('/', function(req, res) {
	var product = Product({
	  name: req.body.name,
	  label: req.body.label,
	  price: req.body.price,
	  config: req.body.config,
	  img: req.body.img
	});
	
	product.save(function(err) {
		if (err) {
			Account.find({ username: req.user.username }, function(err, account) {
			  if (err) throw err;

			  res.render('addproduct', { layout: 'layoutadmin', user : account[0], text : 'Dữ liệu đã tồn tại' });
			});
		}

		Account.find({ username: req.user.username }, function(err, account) {
		  if (err) throw err;

		  res.render('addproduct', { layout: 'layoutadmin', user : account[0] });
		});
	});
});
module.exports = router;
